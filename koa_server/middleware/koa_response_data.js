//处理业务逻辑的中间件，读取某个json文件的数据
const path = require('path');
const fileUtils = require('../utils/file_utils');
module.exports = async (ctx,next) => {
    //根据url进行文件的读取
    const url = ctx.request.url;//对于/api/seller这个路径，对应的文件目录应该是../data/seller.json
    let filePath = url.replace('/api','');// /seller
    filePath = '../data' + filePath + '.json';//...data/seller.json
    filePath = path.join(__dirname,filePath);//__dirname表示当前所在的绝对路径
    try{
        const ret = await fileUtils.getFileJsonData(filePath);
        ctx.response.body = ret;
    }catch (e) {
        const errorMsg = {
            message:'读取文件内容失败，文件资源不存在',
            status:404
        };
        ctx.response.body = JSON.stringify(errorMsg);
    }
    await next();
};