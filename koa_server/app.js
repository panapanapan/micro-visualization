//服务器的入口文件
//1.创建koa实例对象
const Koa = require('koa');
//2.绑定中间件
const app = new Koa();
//绑定第一层中间件
const respDurationMiddleWare = require('./middleware/koa_response_duration');
app.use(respDurationMiddleWare);
//绑定第二层中间件
const respHeaderMiddleWare = require('./middleware/koa_response_header');
app.use(respHeaderMiddleWare);
//绑定第三层中间件
const resDataMiddleWare = require('./middleware/koa_response_data');
app.use(resDataMiddleWare);

//3.绑定端口号：8888
app.listen(8888);

const webSocketService = require('./service/websocket_service')
// 开启服务端的监听，监听客户端的连接
// 当某一个客户端连接成功之后，就会对这个客户端进行message事件的监听
webSocketService.listen()