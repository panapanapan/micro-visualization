[TOC]

## 项目运行

### 启动后端

- cd koa_server
- npm install
- node app.js

### 启动前端

- cd vision
- npm install
- npm run serve

## 项目说明

### 1.项目界面

- 不同的主题切换

  ![image-20210824194829763](README.assets/image-20210824194829763.png)

  ![image-20210824194857253](README.assets/image-20210824194857253.png)

- 多客户端联动

  ![image-20210824194941032](README.assets/image-20210824194941032.png)

  ![image-20210824195001670](README.assets/image-20210824195001670.png)

### 2.项目组件

![前端组件](README.assets/前端组件.png)

### 3.项目技术

![技术](README.assets/技术.png)

